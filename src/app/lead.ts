export class Lead {
    id: number;
    typeOfMove: {};
    status: {};
    client: [ string ];
    paymentLink: string;
    jobTotal: number;
    jobBalance: number;
    customer: {};
    route: [ string ];
    leadJob: {};
    clientNote:	[ string ];
    inventory: [ string ];
    materials: [ string ];
    charges: [ string ];
    routeDistanceTotal:	number;
    hashId: string;
}

export class Charges {
    id:	number;
    name: string;
    price: number;
    floatPrice:	number;
    note: string;
    hideOnFront: boolean;
    isCommissible: boolean;
}

export class Material {
    id:	number;
    name: string;
    price: number;
    isTaxed: boolean;
    type: string;
    showOnFront: boolean;

}
