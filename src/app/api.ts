export const lead = {
  'id': 1,
  'typeOfMove': { 'name': 'Local', 'shortName': 'W' },
  'status': { 'id': 2, 'name': 'Booked', 'slug': 'booked' },
  'client': ['2'],
  'paymentLink': '',
  'jobTotal': 1304,
  'jobBalance': 261.38,
  'customer': {
    'fullName': 'Andrew Drunk',
    'emails': ['andrew.drunk@test.com', 'drunk.andrew@test.com'],
    'phones': ['+380643138592', '+380931234567', '+380991234567']
  },
  'route': [{
    'zip': '01242',
    'type': '0',
    'street': 'Donkey avenue',
    'apartment': '74b',
    'city': 'Austin',
    'state': 'Texas',
    'stairs': 212,
    'elevator': true,
    'doorman': true,
    'accessApartment': false,
    'accessParking': false
  }, {
    'zip': '01242',
    'type': '1',
    'street': 'Donkey avenue',
    'apartment': '7',
    'city': 'Austin',
    'state': 'Texas',
    'stairs': 242,
    'elevator': false,
    'doorman': true,
    'accessApartment': false,
    'accessParking': false
  }, {
    'zip': '01242',
    'type': '0',
    'street': 'Conward street',
    'apartment': '11',
    'city': 'Austin',
    'state': 'Texas',
    'stairs': 222,
    'elevator': false,
    'doorman': true,
    'accessApartment': false,
    'accessParking': true
  }, {
    'zip': '01242',
    'type': '1',
    'street': 'Donkey avenue',
    'apartment': '7',
    'city': 'Austin',
    'state': 'Texas',
    'stairs': 124,
    'elevator': false,
    'doorman': true,
    'accessApartment': true,
    'accessParking': false
  }],
  'leadJob': { 'time': '2018-12-05T16:14:09+0000', 'pickupWindow': 2, 'note': 'Some note' },
  'clientNote': [{ 'message': 'Client message' }],
  'inventory': [
    {
      'name': 'Bedroom',
      'items': [{ 'name': 'Bed', 'volume': 1.5, 'quantity': 2, 'type': 'general' }, {
        'name': 'Chair',
        'volume': 0.5,
        'quantity': 4,
        'type': 'general'
      }]
    }, {
      'name': 'Bathroom',
      'items': [{ 'name': 'Sink', 'volume': 1, 'quantity': 1, 'type': 'general' }]
    }],
  'materials': [
    { 'name': 'Box Small', 'price': 1.5, 'isTaxed': false, 'quantity': 3, 'type': 'general' }
    ],
  'charges': [
    { 'name': 'Charge Small', 'price': 1.5, 'quantity': 1 }
    ],
  'routeDistanceTotal': 27.5,
  'hashId': '1'
};

export const charges = [{
  'id': 2,
  'name': 'Charge Medium',
  'price': 3.2,
  'floatPrice': 3.2,
  'note': 'some note',
  'hideOnFront': false,
  'isCommissible': true
}];

export const materials = [
  {
    'id': 1,
    'name': 'Box Small',
    'price': 1.55,
    'isTaxed': true,
    'type': 'general',
    'showOnFront': true
  }, {
    'id': 3,
    'name': 'Box Large',
    'price': 4,
    'isTaxed': false,
    'type': 'general',
    'showOnFront': true
  }];
