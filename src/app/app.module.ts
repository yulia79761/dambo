import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GeneralInformationComponent } from './pages/general-information/general-information.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { InventotyListComponent } from './pages/inventoty-list/inventoty-list.component';
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ManagerComponent } from './components/manager/manager.component';
import { ActionsComponent } from './pages/general-information/actions/actions.component';
import { AdditionalComponent } from './pages/general-information/additional/additional.component';
import { DataService } from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,
    GeneralInformationComponent,
    PaymentsComponent,
    InventotyListComponent,
    HeaderComponent,
    NavigationComponent,
    AdditionalComponent,
    ManagerComponent,
    ActionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
