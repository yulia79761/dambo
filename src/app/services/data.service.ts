import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Lead, Material, Charges } from '../lead';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {}

  subject = new BehaviorSubject(null);

  getCustomer(leadId: number) {
    return Observable.create(observer => {
      this.http.get<Lead>(`${environment.url}/lead/customer/${leadId}`)
      .pipe(take(1))
      .subscribe(data => {
        observer.next(data);
        this.subject.next(data);
      }, err => console.log(err));
    });
  }

  getCharges() {
    return Observable.create(observer => {
      this.http.get<Charges>(`${environment.url}/lead/charges`)
        .pipe(take(1))
        .subscribe(data => {
          observer.next(data);
          this.subject.next(data);
        }, err => console.log(err));
    });
  }

  getMaterials() {
    return Observable.create(observer => {
      this.http.get<Material>(`${environment.url}/lead/materials`)
        .pipe(take(1))
        .subscribe(data => {
          observer.next(data);
          this.subject.next(data);
        }, err => console.log(err));
    });
  }
}
