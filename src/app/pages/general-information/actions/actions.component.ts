import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit, OnDestroy {

  route: any;
  componentAlive = new Subject();

  constructor(private data: DataService) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.data.getCustomer(1)
    .pipe(takeUntil(this.componentAlive))
    .subscribe(data => {
      this.route = data.route;
    });
  }

  ngOnDestroy() {
    this.componentAlive.next();
  }
}

