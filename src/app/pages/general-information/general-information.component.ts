import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.scss']
})
export class GeneralInformationComponent implements OnInit, OnDestroy {

  today: number = Date.now();

  lead: any;
  route: any;
  componentAlive = new Subject();

  constructor(private data: DataService) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.data.getCustomer(1)
    .pipe(takeUntil(this.componentAlive))
    .subscribe(data => {
      this.lead = data.customer;
    });
  }

  ngOnDestroy() {
    this.componentAlive.next();
  }

}
