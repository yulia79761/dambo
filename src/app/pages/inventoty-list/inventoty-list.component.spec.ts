import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventotyListComponent } from './inventoty-list.component';

describe('InventotyListComponent', () => {
  let component: InventotyListComponent;
  let fixture: ComponentFixture<InventotyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventotyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventotyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
