import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralInformationComponent } from './pages/general-information/general-information.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { InventotyListComponent } from './pages/inventoty-list/inventoty-list.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralInformationComponent
  },
  {
    path: 'payments',
    component: PaymentsComponent
  },
  {
    path: 'inventoty-list',
    component: InventotyListComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
